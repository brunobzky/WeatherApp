import React from 'react';
import PropTypes from 'prop-types';
import WeatherData from './../WeatherLocation/WeatherData';


const ForecastItem = ({ weekDay, hour, data }) => (
    <div>
        <h2>{weekDay} - {hour} hs</h2>
        <WeatherData data={data}></WeatherData>

    </div>
)

ForecastItem.propTypes = {
    weekDay: PropTypes.string,
    hour: PropTypes.number,
    temperature: PropTypes.number,
    weatherState: PropTypes.string,
    humidity: PropTypes.number,
    wind: PropTypes.string,
}

export default ForecastItem;